// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class RevendaVeiculoApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(RevendaVeiculoApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(RevendaVeiculoApplication.class);
	}
}
