// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.servico;

import javax.inject.Named;

import com.eduardo_marcal.revenda_veiculo.dominio.Compra;

@Named public class CompraService extends CrudService<Compra, Long> { }