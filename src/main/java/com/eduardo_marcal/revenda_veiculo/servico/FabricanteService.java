// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.servico;

import javax.inject.Named;

import com.eduardo_marcal.revenda_veiculo.dominio.Fabricante;

/**
 *
 * @author papejajr
 */

@Named
public class FabricanteService extends CrudService<Fabricante, Long> {}