// Agradeço a DEUVenda pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.servico;

import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.eduardo_marcal.revenda_veiculo.dominio.Compra;
import com.eduardo_marcal.revenda_veiculo.dominio.StatusVenda;
import com.eduardo_marcal.revenda_veiculo.dominio.Usuario;
import com.eduardo_marcal.revenda_veiculo.dominio.Venda;
import com.eduardo_marcal.revenda_veiculo.persistencia.VendaRepository;

//@Named public class VendaVendaervice extends CrudVendaervice<Venda, Long> { }
@Service
public class VendaService {

	private VendaRepository repository;
	private @Autowired CompraService compraService;
	private @Autowired PartePagamentoService parteService;

	@Inject
	public void setRepositorio(VendaRepository repository) {
		this.repository = repository;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	public Venda save(Venda objeto) {
		Usuario user = (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		if (user.getCargo().getCargo().equals("Gerente")) {
			objeto.setAutorizador(user);
			objeto.setVendedor(user);
			objeto.setStatus(StatusVenda.AUTORIZADA);
		} else {
			objeto.setVendedor(user);
			objeto.setStatus(StatusVenda.AGUARDANDO_AUTORIZACAO);
		}

		Venda venda = this.repository.save(objeto);
		
		List<Compra> compras = objeto.comprasEnvolvidasVeiculo();

		if (compras.size() > 0)
			compraService.save(compras);
		
		parteService.save(objeto.getPartesPagamento());

		return venda;
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void delete(Iterable<Venda> objetos) {
		this.repository.delete(objetos);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void delete(Long id) {
		this.repository.delete(id);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void delete(Venda objeto) {
		this.repository.delete(objeto);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void deleteAll() {
		this.repository.deleteAll();
	}

	public Venda findOne(Long id) {
		return this.repository.findOne(id);
	}

	public Iterable<Venda> findAll() {
		return this.repository.findAll();
	}

	public Iterable<Venda> findAll(Iterable<Long> ids) {
		return this.repository.findAll(ids);
	}

	public long count() {
		return this.repository.count();
	}

	public boolean exists(Long id) {
		return this.repository.exists(id);
	}

}