// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.servico;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eduardo_marcal.revenda_veiculo.dominio.Modelo;
import com.eduardo_marcal.revenda_veiculo.dominio.Veiculo;
import com.eduardo_marcal.revenda_veiculo.persistencia.VeiculoRepository;

@Service
public class VeiculoService {

	private @Autowired VeiculoRepository repository;

	@Transactional
	public Iterable<Veiculo> save(Iterable<Veiculo> objetos) {
		return this.repository.save(objetos);
	}

	@Transactional
	public Veiculo save(Veiculo objeto) {
		return this.repository.save(objeto);
	}

	@Transactional
	public void delete(Iterable<Veiculo> objetos) {
		this.repository.delete(objetos);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void delete(Long id) {
		this.repository.delete(id);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void delete(Veiculo objeto) {
		this.repository.delete(objeto);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void deleteAll() {
		this.repository.deleteAll();
	}

	@Transactional
	public Veiculo findByPlaca(String placa) {
		return this.repository.findByPlaca(placa);
	}

	@Transactional
	public Iterable<Veiculo> findByAnoFabricacao(Integer anoFabricacao) {
		return this.repository.findByAnoFabricacao(anoFabricacao);
	}

	@Transactional
	public Iterable<Veiculo> findByModelo(Modelo modelo) {
		return this.repository.findByModelo(modelo);
	}

	public Iterable<Veiculo> findAll() {
		return this.repository.findAll();
	}

	public Veiculo findOne(Long id) {
		return this.repository.findOne(id);
	}
}