// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.servico;

import javax.inject.Inject;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.eduardo_marcal.revenda_veiculo.dominio.FormaPagamento;
import com.eduardo_marcal.revenda_veiculo.persistencia.FormaPagamentoRepository;

//@Named public class FormaPagamentoService extends CrudService<FormaPagamento, Long> {}
@Service
public class FormaPagamentoService {

	private FormaPagamentoRepository repository;

	@Inject
	public void setRepositorio(FormaPagamentoRepository repository) {
		this.repository = repository;
	}

	@Transactional
	public Iterable<FormaPagamento> save(Iterable<FormaPagamento> objetos) {
		return this.repository.save(objetos);
	}

	@Transactional
	public FormaPagamento save(FormaPagamento objeto) {
		return this.repository.save(objeto);
	}

	@Transactional
	public void delete(Iterable<FormaPagamento> objetos) {
		this.repository.delete(objetos);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void delete(Long id) {
		this.repository.delete(id);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void delete(FormaPagamento objeto) {
		this.repository.delete(objeto);
	}

	@Transactional
	@PreAuthorize("hasAuthority('ROLE_Gerente')")
	public void deleteAll() {
		this.repository.deleteAll();
	}

	public FormaPagamento findOne(Long id) {
		return this.repository.findOne(id);
	}

	public Iterable<FormaPagamento> findAll() {
		return this.repository.findAll();
	}

	public Iterable<FormaPagamento> findAll(Iterable<Long> ids) {
		return this.repository.findAll(ids);
	}

	public long count() {
		return this.repository.count();
	}

	public boolean exists(Long id) {
		return this.repository.exists(id);
	}

	public FormaPagamento findByDescricao(String descricao) {
		return this.repository.findByDescricao(descricao);
	}

}