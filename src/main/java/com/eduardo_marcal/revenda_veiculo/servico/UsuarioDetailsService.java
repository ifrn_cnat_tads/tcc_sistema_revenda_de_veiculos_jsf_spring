// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.servico;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.eduardo_marcal.revenda_veiculo.dominio.Usuario;
import com.eduardo_marcal.revenda_veiculo.persistencia.UsuarioRepository;

@Component
@Transactional(readOnly = true)
//@Named
public class UsuarioDetailsService implements UserDetailsService {

	@Autowired
	private UsuarioRepository UsuarioRepository;

	@Override
	public UserDetails loadUserByUsername(String cpf) throws UsernameNotFoundException {
		Usuario usuario = UsuarioRepository.findByCpf(cpf);
		if (usuario != null) {
			Collection<GrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority("ROLE_" + usuario.getCargo().getCargo()));
			usuario.setPermissoes(authorities);
			return usuario;
		} else {
			throw new UsernameNotFoundException("Usuário não encontrado");
		}
	}

}
