// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.servico;

import com.eduardo_marcal.revenda_veiculo.dominio.Modelo;

import javax.inject.Named;

/**
 *
 * @author papejajr
 */

@Named
public class ModeloService extends CrudService<Modelo, Long> {
}