// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.FormaPagamento;
import com.eduardo_marcal.revenda_veiculo.servico.FormaPagamentoService;

import lombok.Getter;
import lombok.Setter;

@Controller
@SessionScoped
public class FormaPagamentoBean {

	private @Autowired FormaPagamentoService service;
	private @Getter @Setter FormaPagamento formaPagamento = new FormaPagamento();

	public void save() {
		service.save(formaPagamento);
		resetObject();
	}

	public List<FormaPagamento> list() {
		List<FormaPagamento> formaPagamentos = new ArrayList<>();
		service.findAll().forEach(formaPagamentos::add);
		return formaPagamentos;
	}

	public void drop(Long id) {
		service.delete(id);
		resetObject();
	}

	public void resetObject() {
		formaPagamento = new FormaPagamento();
	}
}