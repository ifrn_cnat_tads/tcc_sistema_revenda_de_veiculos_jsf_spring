// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.Fabricante;
import com.eduardo_marcal.revenda_veiculo.servico.FabricanteService;

import lombok.Getter;
import lombok.Setter;

@Controller
public class FabricanteBean {
	
	@Autowired
	private FabricanteService service;

	@Getter
	@Setter
	private Fabricante fabricante = new Fabricante();

	public String save() {
		service.save(fabricante);
		fabricante = new Fabricante();
		return "/fabricantes/index.jsf?faces-redirect=true";
	}

	public List<Fabricante> list() {
		List<Fabricante> fabricantes = new ArrayList<>();
		service.findAll().forEach(fabricantes::add);
		return fabricantes;
	}

	public void edit() {
		service.save(fabricante);
		fabricante = new Fabricante();
	}

	public void drop(Long id) {
		service.delete(id);
		fabricante = new Fabricante();
	}
}