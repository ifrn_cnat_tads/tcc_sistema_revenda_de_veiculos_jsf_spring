// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.Cargo;
import com.eduardo_marcal.revenda_veiculo.dominio.Usuario;
import com.eduardo_marcal.revenda_veiculo.servico.CargoService;
import com.eduardo_marcal.revenda_veiculo.servico.UsuarioService;

import lombok.Getter;
import lombok.Setter;

@Controller // @Named or @ManagedBean
public class UsuarioBean {

	@Autowired // @Inject
	private UsuarioService usuarioService;
	private @Autowired CargoService cargoService;
	private @Getter Long id;
	// @Setter
	private @Getter Usuario usuario;
	private @Getter @Setter boolean flagEdit;

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
		this.id = usuario.getId();
		flagEdit = true;
	}

	public UsuarioBean() {
		this.usuario = new Usuario();
	}

	public String save() {
		if (flagEdit) {
			Usuario temp = usuarioService.findOne(id);
			// Ajuste dos valores para não ocasionar erro not null
			temp.setNome(usuario.getNome());
			temp.setCpf(usuario.getCpf());
			temp.setTelefone(usuario.getTelefone());
			temp.setStatus(usuario.getStatus());
			temp.setCargo(usuario.getCargo());
			usuarioService.save(temp);
			flagEdit = false;
		} else {
			usuarioService.save(usuario);
		}
		resetObject();
		return "/funcionario/index.jsf?faces-redirect=true";
	}

	public List<Usuario> list() {
		List<Usuario> usuarios = new ArrayList<>();
		usuarioService.findAll().forEach(usuarios::add);
		return usuarios;
	}

	public void drop(Long id) {
		usuarioService.delete(id);
		resetObject();
	}

	public List<Cargo> getCargos() {
		List<Cargo> cargos = new ArrayList<>();
		cargoService.findAll().forEach(cargos::add);
		return cargos;
	}

	public void resetObject() {
		usuario = new Usuario();
	}
}