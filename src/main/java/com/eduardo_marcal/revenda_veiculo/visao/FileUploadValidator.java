// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import javax.servlet.http.Part;

@FacesValidator
public class FileUploadValidator implements Validator {

	private Pattern pattern;
	private Matcher matcher;

	public FileUploadValidator() {
		pattern = Pattern.compile("([^\\s]+(\\.(?i)(jpg|jpeg|png|gif|bmp))$)"); //Espaco nao serve
	}

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		Part file = (Part) value;
		if (file != null)
			if (!checarExtensao(file.getSubmittedFileName()))
				throw new ValidatorException(new FacesMessage("Arquivo não é uma imagem válida"));
	}

	private boolean checarExtensao(String file) {
		matcher = pattern.matcher(file);
		return matcher.matches();
	}
}