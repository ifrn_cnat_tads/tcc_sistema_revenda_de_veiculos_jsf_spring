// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.io.Serializable;

import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.Usuario;
import com.eduardo_marcal.revenda_veiculo.persistencia.UsuarioRepository;

@Controller
@ViewScoped
public class AutenticacaoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	UsuarioRepository usuarioRepository;

	private Authentication authentication;

	public String getUserName() {
		authentication = SecurityContextHolder.getContext().getAuthentication();

		Usuario usuario = usuarioRepository.findByCpf(authentication.getName());
		if (usuario == null)
			return "";
		else
			return usuario.getNome();

		// return usuarioRepository.findByCpf(authentication.getName()).getNome();
	}

	public String logout() {
		SecurityContextHolder.clearContext();
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/login.jsf?faces-redirect=true";
	}
}