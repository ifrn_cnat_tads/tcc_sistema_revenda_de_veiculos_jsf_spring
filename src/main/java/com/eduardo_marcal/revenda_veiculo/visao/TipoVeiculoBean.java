// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.TipoVeiculo;
import com.eduardo_marcal.revenda_veiculo.servico.TipoVeiculoService;

import lombok.Getter;
import lombok.Setter;

@Controller
public class TipoVeiculoBean {
	@Autowired
	private TipoVeiculoService tipoVeiculoService;

	@Getter
	@Setter
	private TipoVeiculo tipoVeiculo = new TipoVeiculo();

	public void save() {
		tipoVeiculoService.save(tipoVeiculo);
		tipoVeiculo = new TipoVeiculo();
	}

	public List<TipoVeiculo> list() {
		List<TipoVeiculo> tipoVeiculos = new ArrayList<>();
		tipoVeiculoService.findAll().forEach(tipoVeiculos::add);
		return tipoVeiculos;
	}

	public void edit() {
		tipoVeiculoService.save(tipoVeiculo);
		tipoVeiculo = new TipoVeiculo();
	}

	public void drop(Long id) {
		tipoVeiculoService.delete(id);
		tipoVeiculo = new TipoVeiculo();
	}
}