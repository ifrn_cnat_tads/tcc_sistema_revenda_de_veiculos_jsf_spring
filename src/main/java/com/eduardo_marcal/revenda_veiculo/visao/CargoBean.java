// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.Cargo;
import com.eduardo_marcal.revenda_veiculo.servico.CargoService;

import lombok.Getter;
import lombok.Setter;

@Controller
@SessionScoped
public class CargoBean {
	
	private @Autowired CargoService service;
	private @Getter @Setter Cargo cargo = new Cargo();

	public void save() {
		service.save(cargo);
		resetObject();
	}

	public void drop(Long id) {
		service.delete(id);
		resetObject();
	}

	public List<Cargo> list() {
		List<Cargo> cargos = new ArrayList<>();
		service.findAll().forEach(cargos::add);
		return cargos;
	}

	public void resetObject() {
		cargo = new Cargo();
	}
}