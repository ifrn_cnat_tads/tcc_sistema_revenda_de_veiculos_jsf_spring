// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.PartePagamento;
import com.eduardo_marcal.revenda_veiculo.servico.PartePagamentoService;

import lombok.Getter;
import lombok.Setter;

@Controller
public class PartePagamentoBean {

	private @Autowired PartePagamentoService service;
	private @Getter @Setter PartePagamento partePagamento;

	public void save() {
		service.save(partePagamento);
		resetObject();
	}

	public List<PartePagamento> list() {
		List<PartePagamento> partePagamentos = new ArrayList<>();
		service.findAll().forEach(partePagamentos::add);
		return partePagamentos;
	}

	public void drop(Long id) {
		service.delete(id);
		resetObject();
	}

	public void resetObject() {
		partePagamento = new PartePagamento();
	}
}