// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import com.eduardo_marcal.revenda_veiculo.dominio.Fabricante;
import com.eduardo_marcal.revenda_veiculo.dominio.Modelo;
import com.eduardo_marcal.revenda_veiculo.dominio.TipoVeiculo;

import com.eduardo_marcal.revenda_veiculo.servico.ModeloService;
import com.eduardo_marcal.revenda_veiculo.servico.FabricanteService;
import com.eduardo_marcal.revenda_veiculo.servico.TipoVeiculoService;

import lombok.Getter;
import lombok.Setter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author papejajr
 */

@Controller
public class ModeloBean
{
    @Autowired
    private ModeloService modeloService;
    
    @Autowired
    private FabricanteService fabricanteService;
    
    @Autowired
    private TipoVeiculoService tipoVeiculoService;
    
    @Getter
    @Setter
    private Modelo modelo = new Modelo();

    public void save()
    {
        modeloService.save(modelo);
        modelo = new Modelo();
    }

    public List<Modelo> list()
    {
        List<Modelo> modelos = new ArrayList<>();
        modeloService.findAll().forEach(modelos::add);
        return modelos;
    }
 
    public void drop(Long id)
    {
        modeloService.delete(id);
    }
        
    public List<Fabricante> getAllFabricantes()
    {
        List<Fabricante> fabricantes = new ArrayList<>();
        fabricanteService.findAll().forEach(fabricantes::add);
        return fabricantes;
    }

    public List<TipoVeiculo> getAllTipoVeiculos()
    {
        List<TipoVeiculo> tipoVeiculos = new ArrayList<>();
        tipoVeiculoService.findAll().forEach(tipoVeiculos::add);
        return tipoVeiculos;
    }
}