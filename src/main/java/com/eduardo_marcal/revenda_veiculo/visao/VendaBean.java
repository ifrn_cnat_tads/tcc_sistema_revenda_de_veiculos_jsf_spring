// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.SessionScoped;
import javax.faces.flow.FlowScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.Compra;
import com.eduardo_marcal.revenda_veiculo.dominio.FormaPagamento;
import com.eduardo_marcal.revenda_veiculo.dominio.PartePagamento;
import com.eduardo_marcal.revenda_veiculo.dominio.Veiculo;
import com.eduardo_marcal.revenda_veiculo.dominio.Venda;
import com.eduardo_marcal.revenda_veiculo.servico.FormaPagamentoService;
import com.eduardo_marcal.revenda_veiculo.servico.VeiculoService;
import com.eduardo_marcal.revenda_veiculo.servico.VendaService;

import lombok.Getter;
import lombok.Setter;
import net.bootsfaces.utils.FacesMessages;

@Controller // @Named or @Controller
@FlowScoped("venda")
@SessionScoped
public class VendaBean implements Serializable {
	private static final long serialVersionUID = 1L;

	// Services
	private @Autowired VeiculoService veiculoService;
	private @Autowired VendaService vendaService;
	private @Autowired FormaPagamentoService formaPagamentoService;

	// Pojo
	private @Getter @Setter PartePagamento partePagamento;
	private @Getter @Setter Veiculo veiculoEnvolvido;
	private @Getter @Setter String placa;
	private @Getter @Setter Venda venda;
	private @Getter @Setter Compra compra;
	private @Getter @Setter Integer qtdCaracteresVenda;
	private @Getter @Setter Integer qtdCaracteresCompra;

	private final int CARACTERES_LENGTH = 200;

	public VendaBean() {
		qtdCaracteresVenda = qtdCaracteresCompra = CARACTERES_LENGTH;
		venda = new Venda();
		partePagamento = new PartePagamento();
	}

	// page: index
	public void pesquisarVeiculoPorPlaca() {
		venda.setVeiculo(veiculoService.findByPlaca(placa));
		placa = new String();
	}

	public void resetObject() {
		venda = new Venda();
		placa = new String();
	}

	// page: dados-venda-flow
	public void observacaoVendaSize() {
		int qtd = venda.getObservacao().length();
		if (qtd >= CARACTERES_LENGTH) {
			FacesMessages.error("Error", "Não é possível cadastrar mais observações");
			return;
		}
		qtdCaracteresVenda = CARACTERES_LENGTH - qtd;
	}

	// page: pagamento-flow
	public List<FormaPagamento> getFormasDePagamento() {
		List<FormaPagamento> formas = new ArrayList<>();
		for (FormaPagamento formaPagamento : formaPagamentoService.findAll())
			if (!formaPagamento.getDescricao().equals("VEÍCULO"))
				formas.add(formaPagamento);
		return formas;
	}

	public void addPartePagamento() {
		partePagamento.setVenda(venda);
		venda.getPartesPagamento().add(partePagamento);
		partePagamento = new PartePagamento();
	}

	public void dropPartePagamento() {
		venda.getPartesPagamento().remove(partePagamento);
	}

	public void resetPartePagamento() {
		partePagamento = new PartePagamento();
	}

	// page: compra-envolvidas-flow
	public void pesquisarVeiculoPorPlacaCompra() {
		veiculoEnvolvido = veiculoService.findByPlaca(placa);
		partePagamento.setCompra(new Compra()); // evitar erro de null
	}

	public void dropPartePagamentoVeiculo() {
		venda.comprasEnvolvidasVeiculo().remove(compra);
	}

	// page: pagamento-veiculo-flow
	public void cancelarPartePagamentoVeiculo() {
		partePagamento = new PartePagamento();
	}

	public void addPartePagamentoVeiculo() {
		partePagamento.setVenda(venda);
		partePagamento.getCompra().setData(new Date());
		partePagamento.getCompra().setVeiculo(veiculoEnvolvido);
		partePagamento.setFormaPagamento(formaPagamentoService.findByDescricao("VEÍCULO"));
		venda.getPartesPagamento().add(partePagamento);
		partePagamento = new PartePagamento();
		placa = new String();
	}

	public void observacaoCompraSize() {
		int qtd = partePagamento.getCompra().getObservacao().length();
		if (qtd >= CARACTERES_LENGTH) {
			FacesMessages.error("Error", "Não é possível cadastrar mais observações");
			return;
		}
		qtdCaracteresCompra = CARACTERES_LENGTH - qtd;
	}

	// page: resumo-flow
	public String save() {
		vendaService.save(venda);
		resetObject();
		return "/venda/index.jsf?faces-redirect=true";
	}
}