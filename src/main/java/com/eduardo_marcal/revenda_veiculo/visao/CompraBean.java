// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.io.Serializable;

import javax.faces.bean.SessionScoped;
import javax.faces.flow.FlowScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.Compra;
import com.eduardo_marcal.revenda_veiculo.servico.CompraService;
import com.eduardo_marcal.revenda_veiculo.servico.VeiculoService;

import lombok.Getter;
import lombok.Setter;
import net.bootsfaces.utils.FacesMessages;

@Controller // @Named or @Controller
@FlowScoped("compra")
@SessionScoped
public class CompraBean implements Serializable {
	private static final long serialVersionUID = 1L;

	// @Autowired or @Inject
	private @Autowired VeiculoService veiculoService;
	private @Autowired CompraService compraService;

	private @Getter @Setter String placa;
	private @Getter @Setter Compra compra;
	private @Getter @Setter Integer qtdCaracteres;
	private final int CARACTERES_LENGTH = 200;
	// private boolean msgCompraRegistadaComSucesso = false;

	private @Getter @Setter String msg = new String();

	public CompraBean() {
		compra = new Compra();
		qtdCaracteres = CARACTERES_LENGTH;
		msg = "Gravado com sucesso";
	}

	public void observacaoSize() {
		int qtd = compra.getObservacao().length();
		if (qtd >= CARACTERES_LENGTH) {
			FacesMessages.error("Error", "Não é possível cadastrar mais observações");
			return;
		}
		qtdCaracteres = CARACTERES_LENGTH - qtd;
	}

	public void pesquisarVeiculoPorPlaca() {
		compra.setVeiculo(veiculoService.findByPlaca(placa));
	}

	public void resetObject() {
		compra = new Compra();
		placa = new String();
	}

	public void save() {
		compraService.save(compra);
		resetObject();
		// Corrigir a mensagem de exibição
		// FacesMessages.info(msg);
		// FacesContext.getCurrentInstance().addMessage("Error", new FacesMessage(msg));
	}
}