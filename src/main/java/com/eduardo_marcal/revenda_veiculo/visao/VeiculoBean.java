// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.visao;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Part;

import net.bootsfaces.utils.FacesMessages;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.eduardo_marcal.revenda_veiculo.dominio.Modelo;
import com.eduardo_marcal.revenda_veiculo.dominio.Veiculo;

import com.eduardo_marcal.revenda_veiculo.servico.ModeloService;
import com.eduardo_marcal.revenda_veiculo.servico.VeiculoService;

import lombok.Getter;
import lombok.Setter;

@Controller
public class VeiculoBean {

	private @Autowired VeiculoService service;
	private @Autowired ModeloService modeloService;
	private @Getter @Setter Veiculo veiculo;
	private @Getter @Setter Part image;
	private @Getter @Setter boolean ano, placa, modelo, search;
	private @Getter @Setter Integer veiculoAno;
	private @Getter @Setter String veiculoPlaca;
	private @Getter @Setter Modelo veiculoModelo;

	public VeiculoBean() {
		this.veiculo = new Veiculo();
	}

	public void setElement(String element) {
		if (element.equals("ano")) {
			ano = true;
			placa = false;
			modelo = false;
		} else if (element.equals("placa")) {
			placa = true;
			ano = false;
			modelo = false;
		} else if (element.equals("modelo")) {
			modelo = true;
			ano = false;
			placa = false;
		}
	}

	public String save() {
		service.save(veiculo);
		resetObject();
		return "/veiculos/create.jsf?faces-redirect=true";
	}

	public List<Veiculo> list() {
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		if (ano) {
			service.findByAnoFabricacao(veiculoAno).forEach(veiculos::add);
		} else if (placa)
			veiculos.add(service.findByPlaca(veiculoPlaca));
		else if (modelo)
			service.findByModelo(veiculoModelo).forEach(veiculos::add);
		else if (search)
			service.findAll().forEach(veiculos::add);

		return veiculos;
	}

	public void drop(Long id) {
		service.delete(id);
		resetObject();
	}

	public void uploadImage() {
		try (InputStream input = image.getInputStream()) {
			byte[] buffer = new byte[8192];
			int bytesRead;
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			while ((bytesRead = input.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
			veiculo.setFoto(output.toByteArray());
			veiculo.setMinitype(image.getContentType());
		} catch (Exception e) {
			FacesMessages.error("Error", "Error ao carregar a imagem");
		}
	}

	public StreamedContent image(byte[] foto) {
		return foto != null ? new DefaultStreamedContent(new ByteArrayInputStream(foto), veiculo.getMinitype())
				: new DefaultStreamedContent();
	}

	public StreamedContent image() {
		return veiculo.getFoto() != null
				? new DefaultStreamedContent(new ByteArrayInputStream(veiculo.getFoto()), veiculo.getMinitype())
				: new DefaultStreamedContent();
	}

	public List<Modelo> getModelos() {
		List<Modelo> modelos = new ArrayList<>();
		modeloService.findAll().forEach(modelos::add);
		return modelos;
	}

	public void resetObject() {
		veiculo = new Veiculo();
		ano = false;
		placa = false;
		modelo = false;
		search = false;
	}
}