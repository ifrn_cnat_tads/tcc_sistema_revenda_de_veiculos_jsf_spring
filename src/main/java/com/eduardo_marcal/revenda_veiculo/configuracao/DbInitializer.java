// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.configuracao;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.eduardo_marcal.revenda_veiculo.dominio.Cargo;
import com.eduardo_marcal.revenda_veiculo.dominio.Fabricante;
import com.eduardo_marcal.revenda_veiculo.dominio.FormaPagamento;
import com.eduardo_marcal.revenda_veiculo.dominio.Modelo;
import com.eduardo_marcal.revenda_veiculo.dominio.TipoVeiculo;
import com.eduardo_marcal.revenda_veiculo.dominio.Usuario;
import com.eduardo_marcal.revenda_veiculo.dominio.Veiculo;
import com.eduardo_marcal.revenda_veiculo.persistencia.CargoRepository;
import com.eduardo_marcal.revenda_veiculo.persistencia.FabricanteRepository;
import com.eduardo_marcal.revenda_veiculo.persistencia.FormaPagamentoRepository;
import com.eduardo_marcal.revenda_veiculo.persistencia.ModeloRepository;
import com.eduardo_marcal.revenda_veiculo.persistencia.TipoVeiculoRepository;
import com.eduardo_marcal.revenda_veiculo.persistencia.UsuarioRepository;
import com.eduardo_marcal.revenda_veiculo.persistencia.VeiculoRepository;

//@Component
public class DbInitializer implements CommandLineRunner {

	@Autowired
	ModeloRepository modeloRepository;
	@Autowired
	FabricanteRepository fabricanteRepository;
	@Autowired
	TipoVeiculoRepository tipoVeiculoRepository;
	@Autowired
	CargoRepository cargoRepository;
	@Autowired
	UsuarioRepository usuarioRepository;
	@Autowired
	VeiculoRepository veiculoRepository;
	@Autowired
	FormaPagamentoRepository formaPagamentoRepository;

	@Override
	public void run(String... arg0) throws Exception {

		// Add Fabricantes
		fabricanteRepository.save(new Fabricante("Chevrolet"));
		fabricanteRepository.save(new Fabricante("Citroen"));
		fabricanteRepository.save(new Fabricante("Fiat"));
		fabricanteRepository.save(new Fabricante("Ford"));
		fabricanteRepository.save(new Fabricante("Honda"));
		fabricanteRepository.save(new Fabricante("Hyundai"));
		fabricanteRepository.save(new Fabricante("Nissan"));
		fabricanteRepository.save(new Fabricante("Mercedes Benz"));
		fabricanteRepository.save(new Fabricante("Toyota"));
		fabricanteRepository.save(new Fabricante("Volkswagen"));

		// Add Tipos de Veículos
		List<TipoVeiculo> tipoVeiculos = Arrays.asList(new TipoVeiculo("Automotor"), new TipoVeiculo("Elétrico"),
				new TipoVeiculo("De propulsão humana"), new TipoVeiculo("De tração animal"),
				new TipoVeiculo("Reboque ou semi-reboque"));
		tipoVeiculoRepository.save(tipoVeiculos);

		// Add Modelos
		List<Modelo> modelos = Arrays.asList(
				new Modelo("Gol", fabricanteRepository.findByMontadora("Volkswagen"),
						tipoVeiculoRepository.findByTipo("Automotor")),
				new Modelo("Ford Fusion Hybrid", fabricanteRepository.findByMontadora("Ford"),
						tipoVeiculoRepository.findByTipo("Elétrico")),
				new Modelo("MB4144 Mercedes Benz", fabricanteRepository.findByMontadora("Mercedes Benz"),
						tipoVeiculoRepository.findByTipo("Reboque ou semi-reboque")),
				new Modelo("Uno", fabricanteRepository.findByMontadora("Fiat"),
						tipoVeiculoRepository.findByTipo("Automotor")),
				new Modelo("Prius", fabricanteRepository.findByMontadora("Toyota"),
						tipoVeiculoRepository.findByTipo("Elétrico")));
		modeloRepository.save(modelos);

		// Add Cargos
		List<Cargo> cargos = Arrays.asList(new Cargo("Gerente"), new Cargo("Vendedor"));
		cargoRepository.save(cargos);

		// Add Usuarios
		usuarioRepository.save(new Usuario("UserManager", "123.456.789-10", "(84) 9 9163-2045", "a", true,
				cargoRepository.findByCargo("Gerente")));

		usuarioRepository.save(new Usuario("UserSeller", "111.111.111-11", "(84) 9 1234-1234", "123", true,
				cargoRepository.findByCargo("Vendedor")));

		// Add Veiculo
		Path file = Paths.get(
				"/home/papejajr/workspace/ifrn/tcc/revenda_veiculos/src/main/resources/images_from_test/fiat-uno-furgao.jpg");
		byte[] foto = Files.readAllBytes(file);

		Veiculo veiculoUno = new Veiculo(2017, "Chassi_UNO", "UNO-2017", new BigDecimal("9.9"),
				modeloRepository.findByDescricao("Uno"), foto, "image/jpg");

		veiculoRepository.save(veiculoUno);

		file = Paths.get(
				"/home/papejajr/workspace/ifrn/tcc/revenda_veiculos/src/main/resources/images_from_test/VW-Gol-2017.jpg");
		;
		foto = Files.readAllBytes(file);

		Veiculo veiculoGol = new Veiculo(2017, "Chassi_GOL", "GOL-2017", new BigDecimal("2.0"),
				modeloRepository.findByDescricao("Gol"), foto, "image/jpg");

		veiculoRepository.save(veiculoGol);

		file = Paths.get(
				"/home/papejajr/workspace/ifrn/tcc/revenda_veiculos/src/main/resources/images_from_test/fiat-uno-furgao.jpg");
		;
		foto = Files.readAllBytes(file);

		Veiculo veiculoFussion = new Veiculo(2018, "Chassi_FUSSION", "FUS-2018", new BigDecimal("2.0"),
				modeloRepository.findByDescricao("Ford Fusion Hybrid"), foto, "image/jpg");

		veiculoRepository.save(veiculoFussion);

		file = Paths.get(
				"/home/papejajr/workspace/ifrn/tcc/revenda_veiculos/src/main/resources/images_from_test/MB4144MercedesBenz.jpeg");
		;
		foto = Files.readAllBytes(file);

		Veiculo veiculoMercedes = new Veiculo(2010, "Chassi_MB4144", "MER-2010", new BigDecimal("5.5"),
				modeloRepository.findByDescricao("MB4144 Mercedes Benz"), foto, "image/jpeg");

		veiculoRepository.save(veiculoMercedes);

		file = Paths.get(
				"/home/papejajr/workspace/ifrn/tcc/revenda_veiculos/src/main/resources/images_from_test/prius_toyota.jpg");
		;
		foto = Files.readAllBytes(file);

		Veiculo veiculoPrius = new Veiculo(2010, "Chassi_Prius", "PRI-2012", new BigDecimal("3.5"),
				modeloRepository.findByDescricao("Prius"), foto, "image/jpg");

		veiculoRepository.save(veiculoPrius);

		// Add Formas de Pagamento
		formaPagamentoRepository.save(new FormaPagamento("ESPÉCIE"));
		formaPagamentoRepository.save(new FormaPagamento("CHEQUE"));
		formaPagamentoRepository.save(new FormaPagamento("VEÍCULO"));
		formaPagamentoRepository.save(new FormaPagamento("DEPÓSITO BANCÁRIO"));
	}
}