// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "vendas")
@SequenceGenerator(sequenceName = "seq_vendas", name = "ID_SEQUENCE", allocationSize = 1)
public class Venda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	@Column(nullable = false)
	private Date data;

	@DecimalMin(value = "0.00", message = "Desconto não permitido")
	@Column(nullable = false)
	private BigDecimal desconto;

	@DecimalMin(value = "0.00", message = "Comissão não permitido")
	@Column(nullable = false)
	private BigDecimal comissao;

	@Column
	@Size(max = 200, message = "Observação com muitos detalhes, favor resumir")
	private String observacao;

	@Column(nullable = false)
	private StatusVenda status;

	@ManyToOne
	@JoinColumn(name = "veiculos_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "veiculo_fk"))
	private Veiculo veiculo;

	@ManyToOne
	@JoinColumn(name = "vendedor_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "usuario_vendedor_fk"))
	private Usuario vendedor;

	@ManyToOne
	@JoinColumn(name = "autorizador_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "usuario_autorizador_fk"))
	private Usuario autorizador;

	@Transient
	private List<PartePagamento> partesPagamento = new ArrayList<>();

	public Venda(Date data, BigDecimal desconto, BigDecimal comissao, String observacao, Veiculo veiculo) {
		super();
		this.data = data;
		this.desconto = desconto;
		this.comissao = comissao;
		this.observacao = observacao;
		this.veiculo = veiculo;
	}

	public BigDecimal valorTotal() {
		BigDecimal total = new BigDecimal("0.00");

		for (PartePagamento p : partesPagamento)
			total = total.add(p.getQuantia());

		return total;
	}

	public List<PartePagamento> partePagamentoSemVeiculo() {
		List<PartePagamento> partes = new ArrayList<>();

		for (PartePagamento p : getPartesPagamento())
			if (!p.getFormaPagamento().getDescricao().equals("VEÍCULO"))
				partes.add(p);

		return partes;
	}

	public List<Compra> comprasEnvolvidasVeiculo() {
		List<Compra> compras = new ArrayList<>();

		for (PartePagamento p : partesPagamento)
			if (p.getFormaPagamento().getDescricao().equals("VEÍCULO"))
				compras.add(p.getCompra());

		return compras;
	}
}