// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "fabricantes")
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(sequenceName = "seq_fabricantes", name = "ID_SEQUENCE", allocationSize = 1)
public class Fabricante implements Serializable, Comparable<Fabricante> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	// @Size(min = 3, message = "Montadora não pode ser inferior a 3 caracteres")
	@Column(nullable = false, unique = true)
	private String montadora;

	public Fabricante(String montadora) {
		this.montadora = montadora;
	}

	@Override
	public int compareTo(Fabricante o) {
		int result = 0;
		if (this.montadora != null && o.montadora != null) {
			result = this.montadora.compareTo(o.montadora);
		} else if (this.montadora == null && o.montadora == null) {
			return result;
		} else if (this.montadora == null) {
			return -1;
		} else {
			return 1;
		}
		return result;
	}
}