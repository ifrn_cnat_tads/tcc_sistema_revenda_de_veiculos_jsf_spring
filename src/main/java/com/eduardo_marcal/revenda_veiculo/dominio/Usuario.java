// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "usuarios")
@SequenceGenerator(sequenceName = "seq_usuarios", name = "ID_SEQUENCE", allocationSize = 1)
public class Usuario implements Serializable, Comparable<Usuario>, UserDetails {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	@Getter
	@Setter
	@Column(nullable = false)
	@Size(min = 3, max = 40, message = "Nome inválido")
	private String nome;

	@Getter
	@Setter
	//@Pattern(regexp="\\d{11}")
	@Column(nullable = false, unique = true)
	@Size(min = 14, max = 14, message = "CPF inválido")
	private String cpf;

	@Getter
	@Setter
	@Column(nullable = false)
	@Size(min = 16, max = 16, message = "Telefone inválido")
	private String telefone;

	@Getter
	@Column(nullable = false)
	private String senha;

	public void setSenha(String senha) {
		this.senha = new BCryptPasswordEncoder().encode(senha);
	}

	@Getter
	@Setter
	@Column(nullable = false)
	private Boolean status;

	@Getter
	@Setter
	@ManyToOne
	@JoinColumn(name = "cargo_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "cargo_fk"))
	private Cargo cargo;

	@Getter
	@Setter
	@Transient
	private Collection<GrantedAuthority> permissoes = new ArrayList<>();

	public Usuario() {
	}

	public Usuario(String nome, String cpf, String telefone, String senha, Boolean status, Cargo cargo) {
		this.nome = nome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.senha = new BCryptPasswordEncoder().encode(senha);
		this.status = status;
		this.cargo = cargo;
	}

	@Override
	public int compareTo(Usuario o) {
		int comparable = 0;
		if (this.cpf != null && o.cpf != null)
			comparable = this.cpf.compareTo(o.cpf);
		else if (this.cpf == null && o.cpf == null)
			return 0;
		else if (this.cpf == null)
			return -1;
		else
			return 1;
		return comparable;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return permissoes;
	}

	@Override
	public String getPassword() {
		return senha;
	}

	@Override
	public String getUsername() {
		return cpf;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return status;
	}

}