// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "modelos")
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_modelos", name = "ID_SEQUENCE", allocationSize = 1)
public class Modelo implements Serializable, Comparable<Modelo> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	// @Size(min = 2, message = "Modelo não pode ser inferior a 2 caracteres")
	@Column(nullable = false)
	private String descricao;

	@ManyToOne
	@JoinColumn(name = "fabricante_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "fabricante_fk"))
	private Fabricante fabricante;

	@ManyToOne
	@JoinColumn(name = "tipo_veiculo_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "tipo_veiculo_fk"))
	private TipoVeiculo tipoVeiculo;

	public Modelo(String descricao, Fabricante fabricante, TipoVeiculo tipoVeiculo) {
		this.descricao = descricao;
		this.fabricante = fabricante;
		this.tipoVeiculo = tipoVeiculo;
	}

	@Override
	public int compareTo(Modelo o) {
		int result = 0;
		if (this.descricao != null && o.descricao != null) {
			result = this.descricao.compareTo(o.descricao);
		} else if (this.descricao == null && o.descricao == null) {
			return result;
		} else if (this.descricao == null) {
			return -1;
		} else {
			return 1;
		}
		return result;
	}
}