// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "forma_pagamento")
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_forma_pagamento", name = "ID_SEQUENCE", allocationSize = 1)
public class FormaPagamento implements Serializable, Comparable<FormaPagamento> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	// @Size(min = 3, max = 30, message = "Descrição inválida")
	@Column(nullable = false, unique = true)
	private String descricao;

	public FormaPagamento(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public int compareTo(FormaPagamento o) {
		int comparable = 0;
		if (this.descricao != null && o.descricao != null)
			comparable = this.descricao.compareTo(o.descricao);
		else if (this.descricao == null && o.descricao == null)
			return 0;
		else if (this.descricao == null)
			return -1;
		else
			return 1;
		return comparable;
	}
}