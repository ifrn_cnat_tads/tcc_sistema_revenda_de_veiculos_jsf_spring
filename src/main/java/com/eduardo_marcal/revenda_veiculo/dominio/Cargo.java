// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "cargos")
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_cargos", name = "ID_SEQUENCE", allocationSize = 1)
public class Cargo implements Serializable, Comparable<Cargo> {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	@Column(nullable = false, unique = true)
	private String cargo;

	public Cargo(String cargo) {
		this.cargo = cargo;
	}

	@Override
	public int compareTo(Cargo o) {
		int comparable = 0;
		if (this.cargo != null && o.cargo != null)
			comparable = this.cargo.compareTo(o.cargo);
		else if (this.cargo == null && o.cargo == null)
			return 0;
		else if (this.cargo == null)
			return -1;
		else
			return 1;
		return comparable;
	}

}
