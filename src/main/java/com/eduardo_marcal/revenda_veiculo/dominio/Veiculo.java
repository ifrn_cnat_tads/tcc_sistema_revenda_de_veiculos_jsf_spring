// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "veiculos")
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_veiculos", name = "ID_SEQUENCE", allocationSize = 1)
public class Veiculo implements Serializable, Comparable<Veiculo> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	// @Min(value = 1900, message = "Ano de Fabricação deve ser maior ou igual à
	// 1900")
	@Column(nullable = false)
	private Integer anoFabricacao;

	@Column(nullable = false, unique = true)
	private String chassi;

	// @Pattern(regexp = "[A-Z]-{3}\\d{4}", message = "Placa fora do padrão")
	@Column(nullable = false, unique = true)
	private String placa;

	@Lob
	@Column(nullable = true)
	private byte[] foto;

	@Column(nullable = true)
	private String minitype;

	// @Digits(integer = 5, fraction = 2, message = "Cilidradas inválida")
	@Column(nullable = false)
	private BigDecimal cilidradas;

	@ManyToOne
	@JoinColumn(name = "modelos_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "tipo_veiculo_fk"))
	private Modelo modelo;

	@Transient
	private StatusVeiculo status;

	public Veiculo(Integer anoFabricacao, String chassi, String placa, BigDecimal cilidradas, Modelo modelo,
			byte[] foto, String minitype) {
		super();
		this.anoFabricacao = anoFabricacao;
		this.chassi = chassi;
		this.placa = placa;
		this.cilidradas = cilidradas;
		this.modelo = modelo;
		this.foto = foto;
		this.minitype = minitype;
	}

	public Veiculo(Integer anoFabricacao, String chassi, String placa, BigDecimal cilidradas, Modelo modelo) {
		super();
		this.anoFabricacao = anoFabricacao;
		this.chassi = chassi;
		this.placa = placa;
		this.cilidradas = cilidradas;
		this.modelo = modelo;
	}

	@Override
	public int compareTo(Veiculo o) {
		int comparable = 0;
		if (this.placa != null && o.placa != null)
			comparable = this.placa.compareTo(o.placa);
		else if (this.placa == null && o.placa == null)
			return 0;
		else if (this.placa == null)
			return -1;
		else
			return 1;
		return comparable;
	}
}