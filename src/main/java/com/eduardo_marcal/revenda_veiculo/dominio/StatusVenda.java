// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

public enum StatusVenda {
	
	AGUARDANDO_AUTORIZACAO("Aguardando autorização"),
	AUTORIZADA("Autorizada"),
	FINALIZADA("Finalizada");
	
	private String status;

	StatusVenda(String status)
	{
		this.status = status;
	}

	public String getStatus()
	{
		return status;
	}
}