// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "parte_pagamento")
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_parte_pagamento", name = "ID_SEQUENCE", allocationSize = 1)
public class PartePagamento implements Serializable, Comparable<PartePagamento> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	@DecimalMin(value = "0.00", message = "Quantia não permitido")
	//@Column(nullable = false)
	private BigDecimal quantia;

	@ManyToOne
	@JoinColumn(name = "forma_pagamento_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "forma_pagamento_fk"))
	private FormaPagamento formaPagamento;

	@ManyToOne
	@JoinColumn(name = "vendas_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "vendas_fk"))
	private Venda venda;

	@ManyToOne
	@JoinColumn(name = "compras_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "compras_fk"))
	private Compra compra;

	public PartePagamento(BigDecimal quantia, FormaPagamento formaPagamento) {
		this.quantia = quantia;
		this.formaPagamento = formaPagamento;
	}

	@Override
	public int compareTo(PartePagamento o) {
		int comparable = 0;
		if (this.quantia != null && o.quantia != null)
			comparable = this.quantia.compareTo(o.quantia);
		else if (this.quantia == null && o.quantia == null)
			return 0;
		else if (this.quantia == null)
			return -1;
		else
			return 1;
		return comparable;
	}
}