// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "compras")
@SequenceGenerator(sequenceName = "seq_compras", name = "ID_SEQUENCE", allocationSize = 1)
public class Compra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	@Column(nullable = false)
	private Date data;

	@DecimalMin(value = "0.00", message = "Quantia não permitido")
	@Column(nullable = false)
	private Double preco;

	@Column
	@Size(max = 200, message = "Observação com muitos detalhes, favor resumir")
	private String observacao;

	@ManyToOne
	@JoinColumn(name = "veiculos_id", referencedColumnName = "id", nullable = false, foreignKey = @ForeignKey(name = "veiculo_fk"))
	private Veiculo veiculo;

	public Compra(Date data, Double preco, String observacao, Veiculo veiculo) {
		super();
		this.data = data;
		this.preco = preco;
		this.observacao = observacao;
		this.veiculo = veiculo;
	}
}