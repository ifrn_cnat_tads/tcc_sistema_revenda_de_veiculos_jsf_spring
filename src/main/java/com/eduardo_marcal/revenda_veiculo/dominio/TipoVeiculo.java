// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tipo_veiculos")
@NoArgsConstructor
@SequenceGenerator(sequenceName = "seq_tipo_veiculos", name = "ID_SEQUENCE", allocationSize = 1)
public class TipoVeiculo implements Serializable, Comparable<TipoVeiculo> {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_SEQUENCE")
	private Long id;

	// @Size(min = 3, message = "O tipo de veículo não pode ser inferior a 3
	// caracteres")
	@Column(nullable = false)
	private String tipo;

	public TipoVeiculo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public int compareTo(TipoVeiculo o) {
		int result = 0;
		if (this.tipo != null && o.tipo != null) {
			result = this.tipo.compareTo(o.tipo);
		} else if (this.tipo == null && o.tipo == null) {
			return 0;
		} else if (this.tipo == null) {
			return -1;
		} else {
			return 1;
		}
		return result;
	}
}