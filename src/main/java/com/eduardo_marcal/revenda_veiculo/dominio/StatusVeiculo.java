// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.dominio;

public enum StatusVeiculo
{
	DISPONIVEL_PARA_VENDA("Disponível para venda"),
	EM_PROCESSO_DE_VENDA("Em processo de venda"),
	NAO_PERTENCE_A_LOJA("Não pertence a loja");

	private String status;

	StatusVeiculo(String status)
	{
		this.status = status;
	}

	public String getStatus()
	{
		return status;
	}
}