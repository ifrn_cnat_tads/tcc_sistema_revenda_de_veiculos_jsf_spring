// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.persistencia;

import com.eduardo_marcal.revenda_veiculo.dominio.PartePagamento;

import org.springframework.data.repository.CrudRepository;

public interface PartePagamentoRepository extends CrudRepository<PartePagamento, Long> {
}