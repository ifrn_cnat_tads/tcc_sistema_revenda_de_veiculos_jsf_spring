// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.persistencia;

import com.eduardo_marcal.revenda_veiculo.dominio.Fabricante;

import org.springframework.data.repository.CrudRepository;

public interface FabricanteRepository extends CrudRepository<Fabricante, Long> {
	Fabricante findByMontadora(String montadora);
}