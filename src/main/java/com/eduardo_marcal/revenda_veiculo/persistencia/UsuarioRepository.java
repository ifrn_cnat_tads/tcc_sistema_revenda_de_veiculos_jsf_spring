// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.persistencia;

import org.springframework.data.repository.CrudRepository;

import com.eduardo_marcal.revenda_veiculo.dominio.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
	Usuario findByCpf(String cpf);
}
