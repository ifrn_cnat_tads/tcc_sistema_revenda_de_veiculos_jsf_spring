// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.persistencia;

import com.eduardo_marcal.revenda_veiculo.dominio.TipoVeiculo;

import org.springframework.data.repository.CrudRepository;

public interface TipoVeiculoRepository extends CrudRepository<TipoVeiculo, Long> {
	TipoVeiculo findByTipo(String tipo);
}