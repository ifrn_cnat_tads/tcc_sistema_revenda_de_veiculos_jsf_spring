// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.persistencia;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.eduardo_marcal.revenda_veiculo.dominio.Modelo;
import com.eduardo_marcal.revenda_veiculo.dominio.Veiculo;

public interface VeiculoRepository extends CrudRepository<Veiculo, Long> {
	Veiculo findByPlaca(String placa);

	List<Veiculo> findByAnoFabricacao(Integer anoFabricacao);

	List<Veiculo> findByModelo(Modelo modelo);
}