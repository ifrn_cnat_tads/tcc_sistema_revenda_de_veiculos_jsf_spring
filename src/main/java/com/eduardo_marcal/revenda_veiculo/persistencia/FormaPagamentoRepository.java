// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.persistencia;

import com.eduardo_marcal.revenda_veiculo.dominio.FormaPagamento;

import org.springframework.data.repository.CrudRepository;

public interface FormaPagamentoRepository extends CrudRepository<FormaPagamento, Long> {
	FormaPagamento findByDescricao(String descricao);
}