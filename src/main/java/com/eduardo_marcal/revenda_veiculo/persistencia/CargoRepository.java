// Agradeço a DEUS pelo dom do conhecimento

package com.eduardo_marcal.revenda_veiculo.persistencia;

import org.springframework.data.repository.CrudRepository;

import com.eduardo_marcal.revenda_veiculo.dominio.Cargo;

public interface CargoRepository extends CrudRepository<Cargo, Long> {
	Cargo findByCargo(String cargo);
}