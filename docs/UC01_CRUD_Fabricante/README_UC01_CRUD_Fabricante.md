# Caso de uso: CRUD Fabricante

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
Este caso de uso tem como pré-condição o usuário do tipo gerente autenticado. Esse usuário do sistema pode realizar as operações de cadastro, edição, exclusão ou consulta de fabricantes de veículos.
</div>

## Implementação do CRUD Fabricante

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
O caso de uso CRUD Fabricante tem como objetivo avaliar o conceito PRG (Post/Redirect/Get) em sua implementação. Ou seja, a implementação deste cenário possibilita avaliar o problema de reenvio de dados no formulário, comum no desenvolvimento web sem uso de Ajax.
</div>

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
Para esse caso, usou a implementação tradicional sem uso de Ajax para criar fabricante, com intuito de observar a diferença do fluxo de execução da requisição *redirect* e *forward*. As demais implementações do crud, como edição e exclusão usam Ajax.
</div>

## Descrição das técnicas e tecnologias adotadas nas implementações dos casos de usos

- Implementação tradicional sem uso de Ajax para a criação de um novo fabricante;
- Implementação usando ajax para edição e exclusão do fabricante;
- Utiliza componentes ricos (Bootsfaces);
- Uso do _redirect_ para evitar duplicidade dos dados no escopo da requisição;
- Utiliza template de página.

## Problema(s) encontrado(s)

- [ 06/11/2017 ] => Criar fabricante

    `O uso do redirect faz necessário para este caso, porque, no forward os dados são mantido no escopo da requisição original, como atributos e parâmetos da requisição. Geralmente, occore nos sequintes métodos do protocolo HTTP: POST e PUT. Com isso torna-se passível de erro ou duplicidade, caso não seja realizado validações, dos dados após o carregamento da página.`

    ![Erro de scope de requisição](images/issue01_error_scope_request.png)

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
Para compreender a diferença e quando utilizar o redirecionamento (redirect) ao invés do encaminhamento (forward). A seguir um exemplo do fluxo de execução será descrito, para melhor entender.
</div>

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
Na figura xx, o campo de entrada está preenchido com a informação "Novo Fabricante (Forward)" e com a exibição da feramenta de inspeção dos elementos do navegador aberta, mas precisamente na aba de rede e na guia HTML, é nesta guia que veremos e/ou compreenderemos o fluxo do encaminhamento e do redirecionamento.
</div>

![Preenchimento do campo de entrada](images/issue01_forward_01.png)

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
Perceba que após o clique no botão "Criar" um novo registro é exibido na tabela e consequentemente a resposta do status (200) HTTP é exibida na parte de inspeção de elementos, ilustrada na figura xx abaixo.
</div>


![Preenchimento do campo de entrada](images/issue01_forward_02.png)

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
Entretanto, após teclar a tecla F5 ou recaregar a página, um alerta é exibido na janela, perguntando se deseja enviar novamente a informações na qual irá repetir a ação, conforme ilustrado na figura xx.
</div>


![Preenchimento do campo de entrada](images/issue01_forward_03.png)

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
Ao realizar tal operação, o sistema poderá duplicar a informação, ilustrada na figura xx, ou até mesmo exibir o(s) erro(s) de validação dos dados, conforme ilustrada na figura xx.
</div>

![Preenchimento do campo de entrada](images/issue01_forward_04.png)

![Preenchimento do campo de entrada](images/issue01_forward_05.png)

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
Para solucionar esse problema comum, usar-se o redirecionamento, da qual é apresentado aseguir, usando o mesmo procedimento. A figura xx abaixo, demostra a primeira ação do cenário.
</div>

![Preenchimento do campo de entrada](images/issue01_redirect_01.png)

<div style="text-align: justify">&nbsp;&nbsp;&nbsp;&nbsp;
Veja que após a submissão do registro a página é redirecionada, conforme ilustrada na parte da inspeção dos elementos.
Percebe-se que o fluxo de resposta se deu da seguinte forma: um redirecionamento HTTP (status 302) e em seguida uma captura dos dados, via o método get (status 200). Isso faz com que o navegador descarte as informações contida no escopo da requisição anterior e quando for solicitar uma nova requisição essa será nova, sem que seja a anterior. Evitando assim, o eventual erro.
</div>

![Preenchimento do campo de entrada](images/issue01_redirect_02.png)

## Interface

![Tela de fabricante](images/issue01_fabricante.png)