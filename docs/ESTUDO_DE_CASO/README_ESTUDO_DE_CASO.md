# TCC

## Estudo de caso de utilização de tecnologias Spring e JSF: implementação de aplicação web para revenda de veículos

Andamento da escrita do Estudo de caso

- [ ] => Resumo
- [ ] => Abstract
- [ ] => Lista de ilustrações
- [ ] => Lista de tabelas
- [ ] => Lista de Abreviaturas e siglas
- [ ] => Introdução
- [ ] => Objetivos
- [X] => Objetivo geral
- [x] => Objetivos específicos
- [ ] => Tecnologias Utilizadas
- [ ] => Java Server Faces
- [ ] => Spring Framework
- [ ] => outras tecnologias adotadas
- [ ] => Estudo de caso
- [x] => Sistema de revenda de veículos
- [ ] => Arquitetura do sistema
- [ ] => Cenários
- [x] => CRUD básico (uma tabela/entidade) sem Ajax
- [x] => CRUD básico (uma tabela/entidade) com Ajax
- [ ] => Upload de arquivo e persistência em banco de dados
- [ ] => Download de arquivo armazenado em banco de dados
- [ ] => Processo com páginas sequenciais (wizard)
- [ ] => Uso de componentes ricos de interface com o usuário
- [ ] => Armazenamento de dados na sessão do usuário
- [ ] => Processo de autenticação do usuário
- [ ] => Operação transacional
- [ ] => Segurança
- [ ] => O que mais você achar interessante
- [ ] => Outras características importantes [ Binding e validação de dados/campos, Suporte a testes automatizados, Criação de operações como serviços REST]
- [ ] => Conclusão