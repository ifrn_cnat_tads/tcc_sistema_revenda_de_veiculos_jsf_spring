# Implementação do caso de uso Veículo

Implementação com uso de ajax para criar, editar e excluir veículo.

## Descrição das técnicas e tecnologias adotadas nas implementações dos casos de usos

- Implementação usando ajax para criar, editar e excluir veículo;
- Utiliza componentes ricos (Bootsfaces);
- Utiliza template de página.

## Problema(s) encontrado(s)

- [ 00/00/0000 ] => Componente Bootsfaces <b:image /> não rederiza imagem base64 apenas url

    `Depois de muita tentativa, e miuta pesquisa sobre a solução a ser aplicacada neste cenário`

    ![Error](images/errors/issueXX_error_xxx.png)

## Interface

![Tela de xxx](images/interfaces/issueXX_xxx.png)