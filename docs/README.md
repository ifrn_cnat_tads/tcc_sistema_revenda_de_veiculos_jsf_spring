# TCC

## Estudo de caso de utilização de tecnologias Spring e JSF: implementação de aplicação web para revenda de veículos

Checklist dos casos de uso implementados.

- [x] #01 => CRUD Fabricante
- [x] #02 => CRUD Tipo de veículo
- [ ] #03 => CRUD Modelo
- [ ] #04 => CRUD Veículo
- [ ] #05 => CRUD Funcionário
- [ ] #06 => Registrar compra
- [ ] #07 => Registrar venda
- [ ] #08 => Autorizar venda
- [ ] #09 => Finalizar venda
- [ ] #10 => Relatório de Veículos mais vendidos
- [ ] #11 => Relatório de comissões

Checklist dos casos de uso documentados.

- [x] #01 => CRUD Fabricante
- [x] #02 => CRUD Tipo de veículo
- [ ] #03 => CRUD Modelo
- [ ] #04 => CRUD Veículo
- [ ] #05 => CRUD Funcionário
- [ ] #06 => Registrar compra
- [ ] #07 => Registrar venda
- [ ] #08 => Autorizar venda
- [ ] #09 => Finalizar venda
- [ ] #10 => Relatório de Veículos mais vendidos
- [ ] #11 => Relatório de comissões

## Problemas encontrados no desenvolvimento

1. Error: `Factory: javax.faces.context.FacesContextFactory not found`
   - __Reportado em__: `09/10/2017` | __Resolvido em__: `09/10/2017`
   - __Contexto da solução__: A integração do JSF + Spring Boot com o RIA do Bootfaces, resultando no erro: Factory: javax.faces.context.FacesContextFactory não encontrado, sendo necessário usar um starter do joinfaces <http://joinfaces.org/> para resolver tal problema. Porém, esse starter esta configurado para exibir as paginas *.xhtml no diretório resources/META-INF, por isso teve que adaptar a estrutura do padrão das aplicações maven web.
