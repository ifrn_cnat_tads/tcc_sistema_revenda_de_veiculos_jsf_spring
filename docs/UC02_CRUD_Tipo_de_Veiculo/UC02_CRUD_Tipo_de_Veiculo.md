# Implementação do CRUD Tipo de Veículo

Implementação com uso de ajax para criar, editar e excluir tipo de veículo.

## Descrição das técnicas e tecnologias adotadas nas implementações dos casos de usos

- Implementação usando ajax para criar, editar e excluir tipo de veículo;
- Utiliza componentes ricos (Bootsfaces);
- Utiliza template de página.

## Problema(s) encontrado(s)

- [ 06/11/2017 ] => Atualização do campo de tipo de veículo

    `Foi necesário adicionar o elemento "@form" no atributo update para que este venha a ser atualizado após a submissão via ajax.`

    ![Campo de entrada não resetado](images/errors/issue02_input_not_updated.png)

## Interface

![Tela de fabricante](images/interfaces/issue02_tipo_veiculos.png)